import { createStackNavigator,createAppContainer,DrawerNavigator,createDrawerNavigator } from 'react-navigation';

import SignUp from './app/component/SignUp';
import Login from './app/component/Login';
import ForgotPassword from './app/component/ForgotPassword';
import SelectOptions from './app/component/SelectOptions';
import Profile from './app/component/Profile';

import ChangePassword from './app/component/ChangePassword';
import ChangePasswordMbl from './app/component/ChangePasswordMbl';
import UpdatePassowrd from './app/component/UpdatePassowrd';
import newprofile from './app/component/newprofile';
import friends from './app/component/friends';
import map from './app/component/map';
import users from './app/component/users';

import home from './app/component/home';
import FriendReq from './app/component/FriendReq';
import privateChat from './app/component/privateChat';
import RoomSelection from './app/component/RoomSelection';
import privateMessage from './app/component/privateMessage';

import singleChat from './app/component/singleChat';
import pop from './app/component/pop';
import ChatApp from './app/component/ChatApp';
import gift from './app/component/gift';


    const AppNavigator = createStackNavigator({
        
         Login :{screen:Login}, 
         SignUp:{screen:SignUp},  
         ForgotPassword:{screen:ForgotPassword},
         SelectOptions:{screen:SelectOptions},
         ChangePassword:{screen:ChangePassword},
         ChangePasswordMbl:{screen:ChangePasswordMbl},
         Profile:{screen:Profile},
         newprofile:{screen:newprofile},
         home:{screen:home},
         privateChat:{screen:privateChat},
         ChatApp :{screen:ChatApp}, 
         gift :{screen:gift},

         FriendReq:{screen:FriendReq},
         UpdatePassowrd:{screen:UpdatePassowrd},
         friends:{screen:friends},
         map:{screen:map},
         users:{screen:users},
         singleChat:{screen:singleChat},
         privateMessage:{screen:privateMessage},
         RoomSelection:{screen:RoomSelection},
         singleChat:{screen:singleChat},
    });


  


const AppContainer = createAppContainer(AppNavigator);

export default AppContainer;
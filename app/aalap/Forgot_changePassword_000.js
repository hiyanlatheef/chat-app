     * "<code>@[+][<i>package</i>:]<i>type</i>/<i>name</i></code>" or a theme
     * attribute in the form
     * "<code>?[<i>package</i>:]<i>type</i>/<i>name</i></code>".
     */
    public static final int actionModeCutDrawable=0x7f020014;
    /**
     * <p>May be a reference to another resource, in the form
     * "<code>@[+][<i>package</i>:]<i>type</i>/<i>name</i></code>" or a theme
     * attribute in the form
     * "<code>?[<i>package</i>:]<i>type</i>/<i>name</i></code>".
     */
    public static final int actionModeFindDrawable=0x7f020015;
    /**
     * <p>May be a reference to another resource, in the form
     * "<code>@[+][<i>package</i>:]<i>type</i>/<i>name</i></code>" or a theme
     * attribute in the form
     * "<code>?[<i>package</i>:]<i>type</i>/<i>name</i></code>".
     */
    public static final int actionModePasteDrawable=0x7f020016;
    /**
     * <p>May be a reference to another resource, in the form
     * "<code>@[+][<i>package</i>:]<i>type</i>/<i>name</i></code>" or a theme
     * attribute in the form
     * "<code>?[<i>package</i>:]<i>type</i>/<i>name</i></code>".
     */
    public static final int actionModePopupWindowStyle=0x7f020017;
    /**
     * <p>May be a reference to another resource, in the form
     * "<code>@[+][<i>package</i>:]<i>type</i>/<i>name</i></code>" or a theme
     * attribute in the form
     * "<code>?[<i>package</i>:]<i>type</i>/<i>name</i></code>".
     */
    public static final int actionModeSelectAllDrawable=0x7f020018;
    /**
     * <p>May be a reference to another resource, in the form
     * "<code>@[+][<i>package</i>:]<i>type</i>/<i>name</i></code>" or a theme
     * attribute in the form
     * "<code>?[<i>package</i>:]<i>type</i>/<i>name</i></code>".
     */
    public static final int actionModeShareDrawable=0x7f020019;
    /**
     * <p>May be a reference to another resource, in the form
     * "<code>@[+][<i>package</i>:]<i>type</i>/<i>name</i></code>" or a theme
     * attribute in the form
     * "<code>?[<i>package</i>:]<i>type</i>/<i>name</i></code>".
     */
    public static final int actionModeSplitBackground=0x7f02001a;
    /**
     * <p>May be a reference to another resource, in the form
     * "<code>@[+][<i>package</i>:]<i>type</i>/<i>name</i></code>" or a theme
     * attribute in the form
     * "<code>?[<i>package</i>:]<i>type</i>/<i>name</i></code>".
     */
    public static final int actionModeStyle=0x7f02001b;
    /**
     * <p>May be a reference to another resource, in the form
     * "<code>@[+][<i>package</i>:]<i>type</i>/<i>name</i></code>" or a theme
     * attribute in the form
     * "<code>?[<i>package</i>:]<i>type</i>/<i>name</i></code>".
     */
    public static final int actionModeWebSearchDrawable=0x7f02001c;
    /**
     * <p>May be a reference to another resource, in the form
     * "<code>@[+][<i>package</i>:]<i>type</i>/<i>name</i></code>" or a theme
     * attribute in the form
     * "<code>?[<i>package</i>:]<i>type</i>/<i>name</i></code>".
     */
    public static final int actionOverflowButtonStyle=0x7f02001d;
    /**
     * <p>May be a reference to another resource, in the form
     * "<code>@[+][<i>package</i>:]<i>type</i>/<i>name</i></code>" or a theme
     * attribute in the form
     * "<code>?[<i>package</i>:]<i>type</i>/<i>name</i></code>".
     */
    public static final int actionOverflowMenuStyle=0x7f02001e;
    /**
     * <p>May be a string value, using '\\;' to escape characters such as
     * '\\n' or '\\uxxxx' for a unicode character;
     */
    public static final int actionProviderClass=0x7f02001f;
    /**
     * <p>May be a string value, using '\\;' to escape characters such as
     * '\\n' or '\\uxxxx' for a unicode character;
     */
    public static final int actionViewClass=0x7f020020;
    /**
     * <p>May be a reference to another resource, in the form
     * "<code>@[+][<i>package</i>:]<i>type</i>/<i>name</i></code>" or a theme
     * attribute in the form
     * "<code>?[<i>package</i>:]<i>type</i>/<i>name</i></code>".
     */
    public static final int activityChooserViewStyle=0x7f020021;
    /**
     * <p>May be a reference to another resource, in the form
     * "<code>@[+][<i>package</i>:]<i>type</i>/<i>name</i></code>" or a theme
     * attribute in the form
     * "<code>?[<i>package</i>:]<i>type</i>/<i>name</i></code>".
     */
    public static final int actualImageResource=0x7f020022;
    /**
     * <p>Must be one of the following constant values.</p>
     * <table>
     * <colgroup align="left" />
     * <colgroup align="left" />
     * <colgroup align="left" />
     * <tr><th>Constant</th><th>Value</th><th>Description</th></tr>
     * <tr><td>center</td><td>4</td><td></td></tr>
     * <tr><td>centerCrop</td><td>6</td><td></td></tr>
     * <tr><td>centerInside</td><td>5</td><td></td></tr>
     * <tr><td>fitBottomStart</td><td>8</td><td></td></tr>
 
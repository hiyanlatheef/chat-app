import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Button,
  TouchableOpacity,
  ImageBackground,
  ScrollView,
  TextInput,
  AsyncStorage,
  ListView, 
  ActivityIndicator, 
   Alert,
   FlatList,
   BackHandler,
   BackAndroid,
} from 'react-native';
import ImagePicker from "react-native-image-picker";
import Icon from 'react-native-vector-icons/FontAwesome5';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Modal from "react-native-modal";

// BackAndroid.exitApp();
export default class home extends Component {

static navigationOptions = ({ navigation }) => {
  return {

    headerBackground: (
      
      <Image
        style={{marginLeft:45,marginTop:5,width:'20%',height:'80%',justifyContent:'center',alignItems:'center',alignSelf:'center'}}
        source={require('./images/logo_web.png')}
      />
    ), 
    headerTitleStyle: {
                textAlign: 'center',
                flexGrow:1,
                alignSelf:'center',
                color:'white',
            },
   
    headerLeft: 
      
      <View style={{flexDirection:'row'}}>
      
      
      <TouchableOpacity  onPress={() => navigation.navigate('friends')}>
        <Icon style={{marginLeft:17,}} 
        name="users" size={26}  color='#69b3f6'
        />
        <Text style={{
           color:"black",
            marginLeft:10,
            textAlign:'center',
            fontSize:12
         }}>Friends </Text>
      </TouchableOpacity>  

      <TouchableOpacity  onPress={() => navigation.navigate('')}>
        <Icon style={{marginLeft:17}} 
        name="gift" size={26}  color='#69b3f6'
        />
         <Text style={{
           color:"black",
            textAlign:'center',
            fontSize:12,
            marginLeft:7
         }} >Rewards </Text>
         </TouchableOpacity>

         <TouchableOpacity  onPress={() => navigation.navigate('home')}>
        <FontAwesome style={{marginLeft:21}} 
        name="envelope-o" size={26}  color='#69b3f6'
        />
         <Text style={{
           color:"black",
            textAlign:'center',
            fontSize:12,
            marginLeft:5, borderBottomWidth:2,
            borderBottomColor:'red',
            marginBottom:-750
         }} >Messages </Text>
          </TouchableOpacity>

          <TouchableOpacity  onPress={() => navigation.navigate('RoomSelection')}>
        <AntDesign style={{marginLeft:30}} 
        name="select1" size={26}  color='#69b3f6'
        />
         <Text style={{
           color:"black",   
           textAlign:'center',
           fontSize:12,
           marginLeft:5
         }} >Room Selection </Text>
         </TouchableOpacity>

        </View>,
  headerRight: 
  
  <View style={{flexDirection:'row'}}>
  
  <TouchableOpacity  onPress={() => navigation.navigate('')}>
  <Ionicons style={{marginLeft:17}} 
        name="md-search" size={26}  color='#69b3f6'
        />
  
    <Text style={{
       color:"black",
        marginLeft:10,
        textAlign:'center',
        fontSize:12
     }}> Search </Text>
  </TouchableOpacity>  

  <TouchableOpacity  onPress={() => navigation.navigate('')}>
  
    <Ionicons style={{marginLeft:29}} 
    name="ios-notifications-outline" size={26}  color='#69b3f6'
    />
     <Text style={{
       color:"black",
        textAlign:'center',
        fontSize:12,
        marginLeft:7
     }} >Notification </Text>
     </TouchableOpacity>

     <TouchableOpacity  onPress={() => navigation.navigate('')}>
     <AntDesign style={{marginLeft:15,}} 
        name="setting" size={26}  color='#69b3f6'
        />
    
     <Text style={{
       color:"black",
        textAlign:'center',
        fontSize:12,
        marginLeft:5
     }} >Settings </Text>
      </TouchableOpacity>

      <TouchableOpacity  onPress={() => navigation.navigate('')}>
      <Icon style={{marginLeft:12
      }} 
        name="user-circle" size={26}  color='#69b3f6'
        />
     <Text style={{
       color:"black",   
       textAlign:'center',
       fontSize:12,
       marginLeft:5
     }} > Profile  </Text>
     </TouchableOpacity>

    </View>,
  }
};

constructor() {
super();
this.state = {

id:'',
email:'',
mobile_no:'',
password:'',
age:'',
dob:'',
screenName :'',
profilePic:'',
referalCode:'', 
gender:'', 
city:'', 
country:'',
tagline:'',
isModalVisible: false,
  isLoading: true,
      text: '',
      img : [],
      usersimg: [],
      usersimg1:[],
      count:'',

}
 this.arrayholder = [] ;
}

screenName = (text) => 
  {
        this.setState({ screenName: text })
  }
  city = (text) => 
  {
        this.setState({ city: text })
  }
  tagline = (text) => 
  {
        this.setState({ tagline: text })
  }








componentWillMount() {
   this.userflatApi();
  this.joinflatApi();
  this.getDetailsApi();
}
  
  async getDetailsApi()
{

var id1 = await AsyncStorage.getItem('userID');

var data={
userID:id1
}

fetch('http://18.204.139.44:3005/usersignupdetail', {
method: 'POST',
headers: {
'Content-Type': 'application/json',
},
body: JSON.stringify(data),
}).then((response) => response.json())
.then((responseJson) => {
console.log(responseJson);
this.setState({
  screenName:responseJson.Result[0].screenName,
  profilePic:responseJson.Result[0].profilePic,
  city:responseJson.Result[0].city, 
  tagline:responseJson.Result[0].tagline, 


})


})
.catch((error) => {
console.error(error);
}); 
}


async userflatApi(){
 var id1 = await AsyncStorage.getItem('userID');

var data={
userID:id1
}



fetch('http://18.204.139.44:3005/usersList', {
method: 'POST',
headers: {
'Content-Type': 'application/json',
},
body: JSON.stringify(data),
}).then((response) => response.json())
.then((responseJson) => {

        this.setState({
          isLoading: false,
          dataSource: responseJson.list,  
          count : responseJson.usercount,

        } 
        );
      })
      .catch((error) => {
        console.error(error);
      
 }); 
}




async joinflatApi(){
  
  var id1 = await AsyncStorage.getItem('userID');

var data={
userID:id1
}


fetch('http://18.204.139.44:3005/usersList', {
method: 'POST',
headers: {
'Content-Type': 'application/json',
},
body: JSON.stringify(data),
}).then((response) => response.json())
.then((responseJson) => {
        this.setState({
          isLoading: false,
          dataSource1: responseJson.list,
         
        } 
        );
      })
      .catch((error) => {
        console.error(error);
      
 }); 
}
_onRefresh = () => {
    this.setState({refreshing: true});
    this.getDetailsApi().then(() => {
      this.setState({refreshing: false});
    });
  }




async actionOnRow(_id) {
this.setState({ isModalVisible: !this.state.isModalVisible });
var receiverId = _id;
  var id1 = await AsyncStorage.getItem('userID');
  AsyncStorage.setItem('receiverId',receiverId);
var data={
userID:id1,
receiverId:receiverId,
}

console.log(data);

fetch('http://18.204.139.44:3005/sendFriendReq', {
method: 'POST',
headers: {
'Content-Type': 'application/json',
},
body: JSON.stringify(data),
}).then((response) => response.json())
.then((responseJson) => {

console.log(responseJson); 

if(responseJson.message == "request send."){
 this.props.navigation.navigate('friends');
}

})
.catch((error) => {
console.error(error);
}); 
    
//     let receiverId = _id;
//     AsyncStorage.setItem('receiverId',receiverId);
//     Alert.alert("This is the receiverId:"+receiverId);
// this.props.navigation.navigate('FriendReq');

}


async frndMethod(){
  this.setState({ isModalVisible: !this.state.isModalVisible });
  this.props.navigation.navigate('friends');
 
}















_toggleModal = () =>
this.setState({ isModalVisible: !this.state.isModalVisible });

 
  //this.props.navigation.navigate('UpdatePassowrd');





  render() {
    return (
 <ImageBackground source={require('./images/Splash_bg.png')} 
 style={{
  width: '100%', 
  height: '100%'}}>
 

   <View style={{flexDirection:"row"}}>
<Text style={{
          color:"white",
          marginLeft:10,
          marginTop:10

         }}>Online Users</Text> 
<Text style={{
          color:"white",
            marginLeft:10,
            marginTop:10,
            width: 25,
            height: 20,
            textAlign:'center',
            backgroundColor: "#CC5B88"
         }}>{this.state
          .count}</Text>
</View>

      <View style={styles.container}>
      
      



      <View style={styles.firstContainer}>
      
<FlatList
          data={this.state.dataSource}
          showsVerticalScrollIndicator={false}
          renderItem={({item}) =>
          
          <TouchableOpacity onPress={this.actionOnRow
            .bind(this, item._id)}
>

        <View style={{flexDirection:"row"}}>
        
       <Image
          style={{
            width: 50, 
            height: 50,
            justifyContent:'flex-start',
            marginTop:"5%",
            alignItems:"center",
            marginTop: '10%',
            marginLeft:"3%",
            borderRadius:25,
            marginBottom:10}}
           source={{uri: 'http://18.204.139.44/AalapApi/uploads/'+item.profilePic}}
         />

        <View style={{marginLeft:5,marginTop:7}} >    
        <Text 
         style={{
          color:"white",
          marginTop:"10%",

          marginLeft:"4%",
          fontSize:15}}>{item.screenName}</Text>
        <Text 
         style={{
          color:"white",
          marginTop:"5%",
          marginLeft:"4%",
          fontSize:15}}>{item.city}</Text>

          </View>
        </View>
      
     </TouchableOpacity>
        }
           keyExtractor={(item,index)=>item.index}
        />
</View>
<Modal isVisible={this.state.isModalVisible}>
          <View style={{ marginLeft:20,height:250,width:300,borderRadius:15,backgroundColor:'white' }}>

        <TouchableOpacity onPress={this._toggleModal}>   
          <Text style={{ marginVertical:20,marginTop:20,marginLeft:30,marginRight:20, fontWeight:"bold",fontSize:14}}> View Profile  </Text>
         </TouchableOpacity>
          <Text style={{  marginVertical:10,marginTop:10,marginLeft:30,marginRight:20, fontWeight:"bold",fontSize:14}}> Chat </Text>
          <Text style={{ marginVertical:20,marginTop:20,marginLeft:30,marginRight:20, fontWeight:"bold",fontSize:14}}> Send Message </Text>
          <TouchableOpacity onPress={() => this.frndMethod()}>  
          <Text style={{  marginVertical:10,marginTop:10,marginLeft:30,marginRight:20, fontWeight:"bold",fontSize:14}}> Add Friend </Text>
          </TouchableOpacity>
         
          <Text style={{ marginVertical:20,marginTop:20,marginLeft:30,marginRight:20, fontWeight:"bold",fontSize:14}}> Report as Spam </Text>
          
  
  </View>
  </Modal>
















<View style={styles.secondContainer}>

<View style={{flexDirection:"row"}}>
<Image source={require('./images/ceo.png')} 
          style={{
            width: 50, 
            height: 50,
            justifyContent:'flex-start',
            marginTop:"5%",
            alignItems:"center",
            marginTop: "5%",
            marginLeft:"3%",
            marginBottom:10,
            borderRadius:20}} />
            
    <View style={{marginLeft:15,marginTop:10}} >    
        <Text 
         style={{
          color:"white",
          marginTop:20,
          marginLeft:"4%",
          fontSize:12,  }}>Kerem suer</Text>
        <Text 
         style={{
          color:"white",
          marginTop:"5%",
          marginLeft:"4%",
          fontSize:12}}>Hey man what's wrong with the....</Text>
    </View>
    </View>
  
      
      
   <View style={{flexDirection:"row"}}>
   <Image source={require('./images/cof.png')} 
          style={{
            width: 50, 
            height: 50,
            justifyContent:'flex-start',
            marginTop:"5%",
            alignItems:"center",
            marginTop: "5%",
            marginLeft:"3%",
            marginBottom:10,
            borderRadius:20}} />
      <View style={{marginLeft:15,marginTop:10}} >    
        <Text 
         style={{
          color:"white",
          marginTop:20,
          marginLeft:"4%",
          fontSize:12, }}>Bill s Kenny</Text>
        <Text 
         style={{
          color:"white",
          marginTop:"5%",
          marginLeft:"4%",
          fontSize:12}}>You need to change two things for...</Text>
        </View>
  </View>
     
  <View style={{flexDirection:"row"}}>
   <Image source={require('./images/cof.png')} 
          style={{
            width: 50, 
            height: 50,
            justifyContent:'flex-start',
            marginTop:"5%",
            alignItems:"center",
            marginTop: "5%",
            marginLeft:"3%",
            marginBottom:10,
            borderRadius:20}} />
      <View style={{marginLeft:15,marginTop:10}} >    
        <Text 
         style={{
          color:"white",
          marginTop:20,
          marginLeft:"4%",
          fontSize:12, }}>Bill s Kenny</Text>
        <Text 
         style={{
          color:"white",
          marginTop:"5%",
          marginLeft:"4%",
          fontSize:12}}>You need to change two things for...</Text>
        </View>
  </View>
  </View>





  


      {/* <FlatList

          data={this.state.dataSource}
          showsVerticalScrollIndicator={false}
          renderItem={({item}) =>
   
        <View style={{flexDirection:"row"}}>
        
       

        <Image
          style={{
            width: 50, 
            height: 50,
            justifyContent:'flex-start',
            marginTop:"5%",
            alignItems:"center",
            marginTop: '10%',
            marginLeft:"3%",
            borderRadius:25,
            marginBottom:10}}
           source={{uri: 'http://18.204.139.44/AalapApi/uploads/'+item.profilePic}}
         />

        <View style={{marginLeft:5,marginTop:7}} >    
        <Text 
         style={{
          color:"white",
          marginTop:"10%",

          marginLeft:"4%",
          fontSize:15}}>{item.screenName}</Text>
        <Text 
         style={{
          color:"white",
          marginTop:"5%",
          marginLeft:"4%",
          fontSize:15}}>{item.city}</Text>



          </View>
        </View>
    
    
        }
           keyExtractor={(item,index)=>item.index}
        /> */}



 









      <View style={styles.thirdContainer}>
       
      <Image source={{ uri:'http://18.204.139.44/AalapApi/uploads/'+this.state.profilePic}} 
          style={{
            width: 50, 
            height: 45,
            justifyContent:'center',
            textAlign:'center',
            marginTop:"10%",
            alignItems:'center',
            marginTop: '18%',
            borderRadius:25,
            marginLeft:"35%",
            borderRadius:25,
            marginBottom:10}} />
        <Text 
         style={{
          color:"white",
          justifyContent:"center",
          alignItems:"center",
          textAlign:"center",
          marginLeft:"4%",
          fontSize:15}}>{this.state.screenName}</Text>
        <Text 
         style={{
          color:"white",
          justifyContent:"center",
          alignItems:"center",
          textAlign:"center",
          marginLeft:"4%",
          fontSize:12}}>{this.state.tagline}</Text> 
        <Text 
         style={{
          color:"white",
          justifyContent:"center",
          alignItems:"center",
          textAlign:"center",
          marginLeft:"4%",
          fontSize:12.5}}>{this.state.city}</Text>     

        <Text style={{
          color:"white",
          justifyContent:"center",
          alignItems:"center",
          textAlign:"center",
          marginLeft:-50,
          fontSize:14}}> Just Joined </Text>


<FlatList
          data={this.state.dataSource1}
          numColumns={2}
          showsVerticalScrollIndicator={false}
          renderItem={({item}) =>
<View style={{flex:1, flexDirection: 'column', margin: 10,}}>
   

     <Image
          style={{
            width: 50, 
            height: 50,
            justifyContent:'center',
            textAlign:'center',
            marginTop:"10%",
            alignItems:'center',
            marginTop: '18%',
            borderRadius:25,
            marginLeft:"16%",
            borderRadius:25,
            marginBottom:10}}
           source={{uri: 'http://18.204.139.44/AalapApi/uploads/'+item.profilePic}}
         />
        <Text style={{
          color:"white",
          justifyContent:"center",
          alignItems:"center",
          textAlign:"center",
          marginLeft:"4%",
          fontSize:15}}>{item.screenName}</Text>
         <Text  style={{
          color:"white",
          justifyContent:"center",
          alignItems:"center",
          textAlign:"center",
          marginLeft:"4%",
          fontSize:15}}>{item.city}</Text>
         </View>
          }
          keyExtractor={item => item.city}
        />





      </View>
          
      </View>
    </ImageBackground>   
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    flexDirection:'row',
  },
  firstContainer:{
    flex:.5,
    backgroundColor:'#25344c',
    marginLeft:-10


  },
  secondContainer:{
    flex:1.5,
    
    
  
  },
  thirdContainer:{
    flex:.5,
    backgroundColor:'#25344c',
    marginTop:-50,
    marginLeft:-150
  },
  loginScreenButton:{
   
    backgroundColor:'white',
   
  },
  loginScreenButton1:{
   
    backgroundColor:'white',
   
  },
  input: {
    margin: 5,
    height: 40,
    width:320,
    padding:10,
    justifyContent:'center',
    borderColor: 'grey',
    borderWidth: 1,
    borderStyle: 'solid', 
    overflow: 'hidden', 
    borderColor: 'grey',
    backgroundColor:'#1f2c3f',
    marginVertical: 12,
    marginTop:25
},

});
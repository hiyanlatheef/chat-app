import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Button,
  TouchableOpacity,
  ImageBackground,
  ScrollView,


  TextInput,
  AsyncStorage,
  ListView, 
  ActivityIndicator, 
   Alert,
   FlatList,
   BackHandler
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import ImagePicker from "react-native-image-picker";
import Icon from 'react-native-vector-icons/FontAwesome5';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { Dropdown } from 'react-native-material-dropdown';
import ModalDropdown from './ModalDropdown';

let data = [{
  value: 'Banana',
}, {
  value: 'Mango',
}, {
  value: 'Pear',
}];


const DEMO_OPTIONS_2 = [
  {"name": "Mumbai Room"},
  {"name": "Delhi Room"},
  {"name": "Pune Room"},
  {"name": "Chennai Room"},
 
];
export default class singleChat extends Component {

static navigationOptions = ({ navigation }) => {
  return {

    headerBackground: (
      <Image
        style={{width:'18%',height:'80%',justifyContent:'center',alignItems:'center',alignSelf:'center',marginTop:5}}
        source={require('./images/logo_web.png')}
      />
    ), 
    headerTitleStyle: {
                textAlign: 'center',
                flexGrow:1,
                alignSelf:'center',
                color:'white',
            },
    
    headerLeft: 
      
      <View style={{flexDirection:'row'}}>
       <Ionicons style={{marginLeft:10}} 
        name="ios-arrow-back" size={26}  color='#69b3f6'
        />
       <TouchableOpacity  onPress={() => navigation.navigate('RoomSelection')}>
      <AntDesign style={{marginLeft:36
      }} 
        name="select1" size={26}  color='#69b3f6'
        />
        
        <Text style={{
           color:"black",
            marginLeft:10,
            textAlign:'center',
            fontSize:12,
            borderBottomWidth:2,
            borderBottomColor:'red',
            marginBottom:-750
         }}> Room Selection </Text>
          
        
      </TouchableOpacity>  
     
      <TouchableOpacity  onPress={() => navigation.navigate('users')}>
        <Icon style={{marginLeft:17}} 
        name="gift" size={26}  color='#69b3f6'
        />
         <Text style={{
           color:"black",
            textAlign:'center',
            fontSize:12,
            marginLeft:7
         }} > Chat </Text>
         </TouchableOpacity>

        </View>
     ,
     headerRight: 
  
     <View style={{flexDirection:'row',marginTop:10}}>
     <TouchableOpacity  onPress={() => navigation.navigate('')}>
     <Ionicons style={{marginLeft:17}} 
           name="md-search" size={26}  color='#69b3f6'
           />
     
       <Text style={{
          color:"black",
           marginLeft:10,
           textAlign:'center',
           fontSize:12
        }}> Search </Text>
     </TouchableOpacity>  
   
     <TouchableOpacity  onPress={() => navigation.navigate('')}>
     
       <Ionicons style={{marginLeft:29}} 
       name="ios-notifications-outline" size={26}  color='#69b3f6'
       />
        <Text style={{
          color:"black",
           textAlign:'center',
           fontSize:12,
           marginLeft:7
        }} >Notification </Text>
        </TouchableOpacity>
   
        <TouchableOpacity  onPress={() => navigation.navigate('')}>
        <AntDesign style={{marginLeft:15,}} 
           name="setting" size={26}  color='#69b3f6'
           />
       
        <Text style={{
          color:"black",
           textAlign:'center',
           fontSize:12,
           marginLeft:5
        }} >Settings </Text>
         </TouchableOpacity>
   
         <TouchableOpacity  onPress={() => navigation.navigate('')}>
         <Icon style={{marginLeft:12
         }} 
           name="user-circle" size={26}  color='#69b3f6'
           />
        <Text style={{
          color:"black",   
          textAlign:'center',
          fontSize:12,
          marginLeft:5
        }} > Profile  </Text>
        </TouchableOpacity>

 {/* <Dropdown 
  style={{
    color:"black",
     marginLeft:50,
  }} 
        // containerStyle={styles.dropdown}
        label='Select Room'
        
        data={data}

      />  */}
   
       </View>,
  }
};

constructor() {
super();
this.state = {

id:'',
email:'',
mobile_no:'',
password:'',
age:'',
dob:'',
screenName :'',
profilePic:'',
referalCode:'', 
gender:'', 
city:'', 
country:'',
tagline:'',
       isLoading: true,
      text: '',
      img : [],
      usersimg: [],
      usersimg1:[],
      img : [],
      usersimg: [],
      usersimg1:[],
      count:'',
    }
    this.arrayholder = [] ;
   }



screenName = (text) => 
  {
        this.setState({ screenName: text })
  }


componentWillMount() {
  
  
  }
  loginApi1(){
    this.props.navigation.navigate('RoomSelection');
  }
  loginApi(){
      
  }
  componentWillMount() {
   
   this.joinflatApi();
   
 }
 async joinflatApi(){
  
    var id1 = await AsyncStorage.getItem('userID');
  
  var data={
  userID:id1
  }
  console.log(data);
  
  fetch('http://18.204.139.44:3005/usersList', {
  method: 'POST',
  headers: {
  'Content-Type': 'application/json',
  },
  body: JSON.stringify(data),
  }).then((response) => response.json())
  .then((responseJson) => {
          this.setState({
            isLoading: false,
            dataSource1: responseJson.list,
           
          } 
          );
        })
        .catch((error) => {
          console.error(error);
        
   }); 
  }

  private(){
    this.props.navigation.navigate('');
  }


  render() {
 
    return (
 <ImageBackground source={require('./images/Splash_bg.png')} 
 style={{
  width: '100%', 
  height: '100%'}}>
 <ScrollView> 
      <View style={styles.container}>
     
    

     <TouchableOpacity    onPress = {() => this.loginApi1()}>
          <LinearGradient style = {styles.loginButton} colors={['#030810', '#04070E']}  >
           
            <Text style = {styles.LoginButtontxt}> Chat Room </Text>
          </LinearGradient>
        </TouchableOpacity>

        <TouchableOpacity    onPress = {() => this.loginApi()}>
          <LinearGradient style = {styles.loginButton1} colors={['#69b3f6', '#25d0de']}>
           
            <Text style = {styles.LoginButtontxt1}> Private Chat </Text>
          </LinearGradient>
        </TouchableOpacity>


        <View style={styles.secondContainer}>
        

       <View style={{flexDirection:"row"}}>
        <Image source={require('./images/ceo.png')} 
          style={{
            width: 50, 
            height: 50,
            justifyContent:'flex-start',
            marginTop:"5%",
            alignItems:"center",
            marginTop: "5%",
            marginLeft:"3%",
            marginBottom:10,
            borderRadius:20}} />

        <View style={{marginLeft:15,marginTop:10}} >    
        <Text 
         style={{
          color:"white",
          marginTop:20,
          marginLeft:"4%",
          fontSize:12,

          }}>Clarke</Text>
        <Text 
         style={{
          color:"white",
          marginTop:"5%",
          marginLeft:"4%",
          fontSize:12}}>Online</Text>
        </View>
       
        </View>
      

        <View style={{flexDirection:"row"}}>
          <Image source={require('./images/auricular-phone-symbol-in-a-circle.png')} 
          style={{
            width: 20, 
            height: 20,
            marginLeft:400,
           }} />
        <Image source={require('./images/facetime-button.png')} 
          style={{
            width: 20, 
            height: 20,
            marginLeft:20,
           }} />
             <Image source={require('./images/hourglass.png')} 
          style={{
            width: 20, 
            height: 20,
            marginLeft:20,
           }} />
        <Image source={require('./images/spam.png')} 
          style={{
            width: 20, 
            height: 20,
            marginLeft:20,
           }} />
</View>



        <View style={{width:"92%", height: 1,marginTop:"1%", backgroundColor: '#4065a5'}} />
     


        <Text 
         style={{
          color:"white",
          marginTop:20,
          marginLeft:"4%",
          fontSize:11,

          }}>Hey!How are you?</Text>
       
       <View style={{flexDirection:"row"}}>
       
        <View style={{marginLeft:15,marginTop:10}} >    
        <Text 
         style={{
          color:"white",
          marginTop:20,
          marginLeft:480,
          fontSize:12,

          }}>Bill s Kenny</Text>
        <Text 
         style={{
          color:"white",
          marginTop:10,
          marginLeft:450,
          fontSize:11}}>Hello!!How are you?</Text>
        </View>
        </View>
        <Text 
         style={{
          color:"white",
          marginTop:20,
          marginLeft:"4%",
          fontSize:11,

          }}>I'm doing great</Text>
<View  style={{flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop:15,
    marginBottom:15}}>
<TextInput
          style={{
            marginTop:20,
            borderBottomWidth:1, 
            width:'100%', 
            padding:10, 
            height:50,
            marginLeft:-20, 
            color: "grey",
            borderWidth: 1,
            backgroundColor: '#1D3036',
            borderRadius: 25,
            }}
            placeholder="Type your text here..."
          editable={true}
          onChangeText = {this.email}
          value = {this.state.email}
          />  
            

     

 





</View>

</View>

           
      </View>
      </ScrollView>
    </ImageBackground>   
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    flexDirection:'row',
  },
  firstContainer:{
    flex:.7,
    

  },
  secondContainer:{
    flex:1.5,
    marginTop:50,
    marginLeft:-280
  
  },
  thirdContainer:{
    flex:.7,
      backgroundColor:'#25344c',
  },
  input: {
    margin: 5,
    height: 40,
    width:320,
    padding:10,
    justifyContent:'center',
    borderColor: 'grey',
    borderWidth: 1,
    borderStyle: 'solid', 
    overflow: 'hidden', 
    borderColor: 'grey',
    backgroundColor:'#1f2c3f',
    marginVertical: 12,
    marginTop:25
},
loginButton: {
    margin: 5,
    height: 35,
    width:"80%",
    padding:10,
    borderColor: 'grey', 
    marginVertical: 10,
    marginLeft:35,
    marginTop:10,
    borderRadius:18,
   },

  LoginButtontxt:{
    color: 'white',
    padding: 2,
    marginLeft:20,
    marginTop:-5
   },
   loginButton1: {
    margin: 5,
    height: 35,
    width:"75%",
    padding:10,
    borderColor: 'grey', 
    marginVertical: 10,
    marginLeft:35,
    marginTop:10,
    borderRadius:18,
   },

  LoginButtontxt1:{
    color: 'white',
    padding: 2,
    marginLeft:10,
    marginTop:-5
   },
  //  dropdown:{
  //    marginLeft:50,
  //    width:'100%',
  //    backgroundColor:'red'
  //  }

});
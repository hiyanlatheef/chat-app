import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Button,
  TouchableOpacity,
  ImageBackground,
  ScrollView,
  TextInput,
  AsyncStorage,
  ListView, 
  ActivityIndicator, 
   Alert,
   FlatList,
   BackHandler
} from 'react-native';
import ImagePicker from "react-native-image-picker";
import Icon from 'react-native-vector-icons/FontAwesome5';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import Ionicons from 'react-native-vector-icons/Ionicons';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';


export default class users extends Component {

static navigationOptions = ({ navigation }) => {
  return {
    headerBackground: (
      <Image
        style={{marginLeft:80,marginTop:5,width:'16%',height:'80%',justifyContent:'center',alignItems:'center',alignSelf:'center'}}
        source={require('./images/logo_web.png')}
      />
    ), 
    headerTitleStyle: {
                textAlign: 'center',
                flexGrow:1,
                alignSelf:'center',
                color:'white',
            },
    
    headerLeft: 
      
      <View style={{flexDirection:'row'}}>
      <TouchableOpacity  onPress={() => navigation.navigate('RoomSelection')}>
      <Ionicons style={{marginLeft:10}} 
        name="ios-arrow-back" size={26}  color='#69b3f6'
        />
        </TouchableOpacity>
      
      <TouchableOpacity  onPress={() => navigation.navigate('RoomSelection')}>
      <AntDesign style={{marginLeft:36
      }} 
        name="select1" size={26}  color='#69b3f6'
        />
        
        <Text style={{
           color:"black",
            marginLeft:10,
            textAlign:'center',
            fontSize:12
         }}> Room Selection </Text>
      </TouchableOpacity>  

      <TouchableOpacity  onPress={() => navigation.navigate('users')}>
        <Icon style={{marginLeft:17}} 
        name="gift" size={26}  color='#69b3f6'
        />
         <Text style={{
           color:"black",
            textAlign:'center',
            fontSize:12,
            marginLeft:7,
            borderBottomWidth:2,
            borderBottomColor:'red',
            marginBottom:-750
         }} > Chat </Text>
         </TouchableOpacity>

         <TouchableOpacity  onPress={() => navigation.navigate('')}>
        <EvilIcons style={{marginLeft:25}} 
        name="sc-telegram" size={35}  color='#69b3f6'
        />
         <Text style={{
           color:"black",
            textAlign:'center',
            fontSize:12,
            marginLeft:5,
           
         }} > Map View </Text>
          </TouchableOpacity>

          <TouchableOpacity  onPress={() => navigation.navigate('')}>
        <FontAwesome5 style={{marginLeft:15}} 
        name="list-alt" size={26}  color='black'
        />
         <Text style={{
           color:"black",   
           textAlign:'center',
           fontSize:12,
           marginLeft:5
         }} > List View  </Text>
         </TouchableOpacity>

        </View>,
  headerRight: 
  
  <View style={{flexDirection:'row'}}>
      
      
  <TouchableOpacity  onPress={() => navigation.navigate('')}>
  <Ionicons style={{marginLeft:17}} 
        name="md-search" size={26}  color='#69b3f6'
        />
  
    <Text style={{
       color:"black",
        marginLeft:10,
        textAlign:'center',
        fontSize:12
     }}> Search </Text>
  </TouchableOpacity>  

  <TouchableOpacity  onPress={() => navigation.navigate('')}>
  
    <Ionicons style={{marginLeft:17}} 
    name="ios-notifications-outline" size={26}  color='#69b3f6'
    />
     <Text style={{
       color:"black",
        textAlign:'center',
        fontSize:12,
        marginLeft:7
     }} >Notification </Text>
     </TouchableOpacity>

     <TouchableOpacity  onPress={() => navigation.navigate('')}>
     <AntDesign style={{marginLeft:15,}} 
        name="setting" size={26}  color='#69b3f6'
        />
    
     <Text style={{
       color:"black",
        textAlign:'center',
        fontSize:12,
        marginLeft:5
     }} >Settings </Text>
      </TouchableOpacity>

      <TouchableOpacity  onPress={() => navigation.navigate('')}>
      <Icon style={{marginLeft:28
      }} 
        name="user-circle" size={26}  color='#69b3f6'
        />
     <Text style={{
       color:"black",   
       textAlign:'center',
       fontSize:12,
       marginLeft:5
     }} > Profile  </Text>
     </TouchableOpacity>

    </View>,
  }
};

constructor() {
super();
this.state = {

id:'',
email:'',
mobile_no:'',
password:'',
age:'',
dob:'',
screenName :'',
profilePic:'',
referalCode:'', 
gender:'', 
city:'', 
country:'',
tagline:'',
       isLoading: true,
      text: '',
      img : [],
      usersimg: [],
      usersimg1:[],
      count:'',


}
}

screenName = (text) => 
  {
        this.setState({ screenName: text })
  }


componentWillMount() {
  //this.getDetailsApi();
  this.userflatApi();
  
  }


  async userflatApi(){
    var id1 = await AsyncStorage.getItem('userID');
   
   var data={
   userID:id1
   }
   
   console.log(data);
   
   fetch('http://18.204.139.44:3005/usersList', {
   method: 'POST',
   headers: {
   'Content-Type': 'application/json',
   },
   body: JSON.stringify(data),
   }).then((response) => response.json())
   .then((responseJson) => {
   
           this.setState({
             isLoading: false,
             dataSource: responseJson.list,  
             count : responseJson.usercount,
   
           } 
           );
         })
         .catch((error) => {
           console.error(error);
         
    }); 
   }
//   async getDetailsApi()
// {

// var id1 = await AsyncStorage.getItem('userID');

// var data={
// userID:id1
// }
// console.log(data);
// fetch('http://18.204.139.44:3005/usersignupdetail', {
// method: 'POST',
// headers: {
// 'Content-Type': 'application/json',
// },
// body: JSON.stringify(data),
// }).then((response) => response.json())
// .then((responseJson) => {
// console.log(responseJson);
// this.setState({
//   screenName:responseJson.Result[0].screenName,
//   profilePic:responseJson.Result[0].profilePic 

// })


// })
// .catch((error) => {
// console.error(error);
// }); 
// }

async actionOnRow(_id) {
  this.props.navigation.navigate('RoomSelection');

}

_onRefresh = () => {
    this.setState({refreshing: true});
    this.getDetailsApi().then(() => {
      this.setState({refreshing: false});
    });
  }


  render() {
 if (this.state.isLoading) {
      return (
        <View style={{flex: 1, paddingTop: 20}}>
          <ActivityIndicator />
        </View>
      );
    }
    return (
 <ImageBackground source={require('./images/Splash_bg.png')} 
 style={{
  width: '100%', 
  height: '100%'}}>



      <View style={styles.container}>
     
      <FlatList
       numColumns={4}
          data={this.state.dataSource}
          showsVerticalScrollIndicator={false}
          renderItem={({item}) =>
          <TouchableOpacity onPress={this.actionOnRow
            .bind(this, item._id)}>
          
<View style={styles.firstContainer}>
<View style={{flexDirection:"row",marginLeft:10}}>

        <Image
          style={{
            width: 50, 
            height: 50,
            justifyContent:'flex-start',
            marginTop:"5%",
            alignItems:"center",
            marginTop: '10%',
            marginLeft:"3%",
            borderRadius:25,
            marginBottom:10}}
           source={{uri: 'http://18.204.139.44/AalapApi/uploads/'+item.profilePic}}
         />
<View style={{marginLeft:5,marginTop:7}} >  

        <Text style={{
          color:"white",
          marginTop:"10%",
          marginLeft:"4%",
          fontSize:10}}>{item.screenName}</Text>
       <Text style={{
          color:"white",
          marginTop:"5%",
          marginLeft:"4%",
          fontSize:10}}>{item.city}</Text>
         
  </View>
  </View>
   </View>
     
     </TouchableOpacity>
          }
          keyExtractor={item => item.screenName}
        />


           
      </View>
    </ImageBackground>   
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    flexDirection:'row',
  },
  firstContainer:{
    flex:.7,
    

  },
  secondContainer:{
    flex:1.5,
  
  },
  thirdContainer:{
    flex:.7,
      backgroundColor:'#25344c',
  },
  input: {
    margin: 5,
    height: 40,
    width:320,
    padding:10,
    justifyContent:'center',
    borderColor: 'grey',
    borderWidth: 1,
    borderStyle: 'solid', 
    overflow: 'hidden', 
    borderColor: 'grey',
    backgroundColor:'#1f2c3f',
    marginVertical: 12,
    marginTop:25
},

});
import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Button,
  TouchableOpacity,
  ImageBackground,
  ScrollView,
  TextInput,
  AsyncStorage,
 
} from 'react-native';
import ImagePicker from "react-native-image-picker";
import Icon from 'react-native-vector-icons/FontAwesome5';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import Ionicons from 'react-native-vector-icons/Ionicons';

export default class map extends Component {

static navigationOptions = ({ navigation }) => {
  return {

    headerBackground: (
      <Image
        style={{width:'30%',height:'100%',justifyContent:'center',alignItems:'center',alignSelf:'center'}}
        source={require('./images/logo_web.png')}
      />
    ), 
    headerTitleStyle: {
                textAlign: 'center',
                flexGrow:1,
                alignSelf:'center',
                color:'white',
            },
    
    headerLeft: 
      <TouchableOpacity style={ [{}] }>
      <View style={{flexDirection:'row'}}>
        <Icon style={{marginLeft:15,}} 
        name="users" size={26}  color='#69b3f6'
        />
        <Icon style={{marginLeft:25}} 
        name="gift" size={26}  color='#69b3f6'
        />
        <FontAwesome style={{marginLeft:25}} 
        name="envelope-o" size={26}  color='#69b3f6'
        
        />
        <AntDesign style={{marginLeft:25}} 
        name="select1" size={26}  color='#69b3f6'
        
        />
        </View>
     </TouchableOpacity>,
  headerRight: 
   <TouchableOpacity style={ [{}] }>
      <View style={{flexDirection:'row',justifyContent:'space-around'}}>
        <Ionicons style={{marginLeft:55}} 
        name="md-search" size={28}  color='#69b3f6'
        onPress={() =>navigation.navigate('DrawerToggle')}
        />
        <Ionicons style={{marginLeft:25}} 
        name="ios-notifications-outline" size={28}  color='#69b3f6'
        onPress={() =>navigation.navigate('DrawerToggle')}
        />
        <AntDesign style={{marginLeft:15,}} 
        name="setting" size={28}  color='#69b3f6'
        onPress={() =>navigation.navigate('DrawerToggle')}
        />
        <Icon style={{marginLeft:25}} 
        name="user-circle" size={28}  color='#69b3f6'
        onPress={() =>navigation.navigate('DrawerToggle')}
        />

        </View>
     </TouchableOpacity>, 
  }
};

constructor() {
super();
this.state = {

id:'',
email:'',
mobile_no:'',
password:'',
age:'',
dob:'',
screenName :'',
profilePic:'',
referalCode:'', 
gender:'', 
city:'', 
country:'',
tagline:'',
txtnum:'55',

}
}

screenName = (text) => 
  {
        this.setState({ screenName: text })
  }

componentWillMount() {
  this.getDetailsApi();
  }
  
  async getDetailsApi()
{

var id1 = await AsyncStorage.getItem('_id');

var data={
_id:id1
}
console.log(data);
fetch('http://18.204.139.44:3005/usersignupdetail', {
method: 'POST',
headers: {
'Content-Type': 'application/json',
},
body: JSON.stringify(data),
}).then((response) => response.json())
.then((responseJson) => {
console.log(responseJson);
this.setState({
  screenName:responseJson.Result[0].screenName,
  profilePic:responseJson.Result[0].profilePic 

})


})
.catch((error) => {
console.error(error);
}); 
}

_onRefresh = () => {
    this.setState({refreshing: true});
    this.getDetailsApi().then(() => {
      this.setState({refreshing: false});
    });
  }

butt(){
  var bt = "25";
}
  render() {
    return (
 
 
      <View style={styles.container}>
      
     
      </View>
   
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    flexDirection:'row',
  },
  firstContainer:{
    flex:.7,
    backgroundColor:'#25344c',

  },
  secondContainer:{
    flex:1.5,
  
  },
  thirdContainer:{
    flex:.7,
      backgroundColor:'#25344c',
  },
  input: {
    margin: 5,
    height: 40,
    width:320,
    padding:10,
    justifyContent:'center',
    borderColor: 'grey',
    borderWidth: 1,
    borderStyle: 'solid', 
    overflow: 'hidden', 
    borderColor: 'grey',
    backgroundColor:'#1f2c3f',
    marginVertical: 12,
    marginTop:25
},
 text: {
      marginLeft:10,
      marginTop:15,
      height: 20,
      width: 25,
      backgroundColor: '#F470A4',
      color:'white',
      textAlign:'center'
   }

});